﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using RSA.Models;
using RSA.ServRSA;
using RSA.VM;
using System.IO;
using System.Net;

namespace RSA.Controllers
{
    public class GaleriaController : Controller
    {
        GaleriaModel moduloModel = new GaleriaModel();

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AllGalerias(ServRSA.VMGridGaleriaGet modelo)
        {
            ServRSA.VMTableOfVMGaleriai0e4Zjb6 model = moduloModel.CatalogoGaleria(modelo);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ServRSA.VMGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = new GaleriaModel().RegistrarGaleria(model);
                resp.success = true;
                resp.alerta = "Galería registrada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Details(int id)
        {
            ServRSA.VMGaleria model = new GaleriaModel().GetGaleria(id);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(ServRSA.VMGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.EditarGaleria(model);
                resp.success = true;
                resp.alerta = "Galería actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(ServRSA.VMGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                new GaleriaModel().EliminarGaleria(model.IdGaleria);
                resp.success = true;
                resp.alerta = "Galería inactivada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        #region Detalle Galerias 

        [HttpPost]
        public JsonResult AllImages(ServRSA.VMGridImagenGaleriaGet modelo)
        {
            
            ServRSA.VMTableOfVMImagenGaleriai0e4Zjb6 model = moduloModel.CatalogoImagenGaleria(modelo);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetImages(bool todas = true)
        {
            ServRSA.VMListImagenGaleria[] model = moduloModel.ObtenerImagesGaleria(todas);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string UploadImage(HttpPostedFileBase filepond, int id)
        {
            if (filepond.ContentLength <= 0)
                throw new Exception("Error mientras se cargaba el archivo");
            //string extension = filepond.FileName.Substring(filepond.FileName.LastIndexOf(".", StringComparison.Ordinal));
            //byte[] archivo = new byte[filepond.InputStream.Length];
            //filepond.InputStream.Read(archivo, 0, (int)filepond.InputStream.Length - 1);
            //ServRSA.VMImagenGaleria model = moduloModel.GetImageGaleria(id);
            //model.BinaryImagen = archivo;
            //model.TipoImagen = filepond.ContentType;
            //this.UpdateImage(model);


            string extension = filepond.FileName.Substring(filepond.FileName.LastIndexOf("."));
            string fileName = "_" + id.ToString() + extension;
            byte[] archivo = new byte[filepond.InputStream.Length];
            filepond.InputStream.Read(archivo, 0, (int)filepond.InputStream.Length - 1);
            ServRSA.VMImagenGaleria model = moduloModel.GetImageGaleria(id);
            model.BinaryImagen = archivo;
            model.TipoImagen = extension; //filepond.ContentType;
            this.UpdateImage(model);
            if (!Directory.Exists(Server.MapPath("~/Images/galerias/_"+model.IdGaleria)))
            {
                Directory.CreateDirectory(Server.MapPath("~/Images/galerias/_" + model.IdGaleria));
            }
            filepond.SaveAs(Server.MapPath("~/Images/galerias/_" + model.IdGaleria+"/" + fileName));
            

            return "Imagen cargada correctamente";
        }

        [HttpPost]
        public JsonResult CreateImage(ServRSA.VMImagenGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                //Por ahora el usuario se coloca fijo
                model.IdUsuarioRegistra = 10001;
                resp.result = new GaleriaModel().RegistrarImageGaleria(model);
                resp.success = true;
                resp.alerta = "Imagen registrada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult UpdateImage(ServRSA.VMImagenGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.ActualizarImageGaleria(model);
                resp.success = true;
                resp.alerta = "Imagen actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteImage(VMImagenGaleria imgGaleria)
        {

            RespuestaModel resp = new RespuestaModel();
            try
            {
                new GaleriaModel().EliminarImageGaleria(imgGaleria.IdImagen);
                if (Directory.Exists(Server.MapPath("~/Images/galerias/_" + imgGaleria.IdGaleria)))
                {
                    string fileName = "_" + imgGaleria.IdImagen + imgGaleria.TipoImagen;
                    string filenamecomplete = Server.MapPath("~/Images/galerias/_" + imgGaleria.IdGaleria + "/" + fileName);
                    if (System.IO.File.Exists(filenamecomplete))
                    {
                        System.IO.File.Delete(filenamecomplete);
                    }
                }
                resp.success = true;
                resp.alerta = "Imagen eliminada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InactivateImage(ServRSA.VMImagenGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                new GaleriaModel().BajaImageGaleria(model.IdImagen);
                resp.success = true;
                resp.alerta = "Imagen inactivada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DetailsImage(int id)
        {
            ServRSA.VMImagenGaleria model = new GaleriaModel().GetImageGaleria(id);

            // Crea un nuevo modelo solo para enviar el codigo bynario en base 64,
            // se podria hacer desde el cliente con JS, pero al ser imagenes cargaria mucho el motor del explorador.
            return Json(new { model = new {
                BinaryImagen = Convert.ToBase64String(  model.BinaryImagen),
                IdImagen = model.IdImagen,
                TipoImagen = model.TipoImagen
            } }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}