﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSA.Models;
using RSA.VM;
using System.IO;

namespace RSA.Controllers
{
    public class AvisoController : Controller
    {
        AvisoModel moduloModel = new AvisoModel();

        // GET: Aviso
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AllAviso(ServRSA.VMGridAvisoGet modelo)
        {
            ServRSA.VMTableOfVMAvisoi0e4Zjb6 model = moduloModel.CatalogoAviso(modelo);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ServRSA.VMAviso model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = moduloModel.RegistrarAviso(model);
                resp.success = true;
                resp.alerta = "Aviso registrado correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Details(int id)
        {
            ServRSA.VMAviso model = moduloModel.GetAviso(id);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(ServRSA.VMAviso model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.EditarAviso(model);
                resp.result = model;
                resp.success = true;
                resp.alerta = "Información actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Baja(int id)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.BajaAviso(id);
                resp.success = true;
                resp.alerta = "Información actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string UploadImage(HttpPostedFileBase filepond, int id)
        {
            if (filepond.ContentLength <= 0)
            {
                return "Error mientras se cargaba el archivo";
            }
            else
            {
                string extension = filepond.FileName.Substring(filepond.FileName.LastIndexOf("."));
                string fileName = "aviso_"+ DateTime.Now.Ticks.ToString()+ "_" + id.ToString() + extension;
                //byte[] archivo = new byte[filepond.InputStream.Length];
                //filepond.InputStream.Read(archivo, 0, (int)filepond.InputStream.Length - 1);
                ServRSA.VMAviso model = moduloModel.GetAviso(id);
                if (System.IO.File.Exists(Server.MapPath("~/Images/avisos/" + model.NombreImagen)))
                {
                    System.IO.File.Delete(Server.MapPath("~/Images/avisos/" + model.NombreImagen));
                }
                    //model.ImagenBinary = archivo;
                model.NombreImagen = fileName; 
                this.Update(model);                            
                if(System.IO.File.Exists(Server.MapPath("~/Images/avisos/" + fileName)))
                {
                    System.IO.File.Delete(Server.MapPath("~/Images/avisos/" + fileName));
                }
                filepond.SaveAs(Server.MapPath("~/Images/avisos/"+fileName));
                return "Imagen cargada correctamente";
            }
        }

        [HttpPost]
        public JsonResult ListAviso(bool todos = true)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = moduloModel.ListarAviso(todos);
                resp.success = true;
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }
    }
}