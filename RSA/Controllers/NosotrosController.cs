﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSA.Controllers
{
    public class NosotrosController : Controller
    {
        // GET: Nosotros
        public ActionResult Index()
        {
            return View();
        }

        [ActionName("quienes-somos")]
        public ActionResult QuienesSomos()
        {
            return View("QuienesSomos");
        }

        [ActionName("historia")]
        public ActionResult Historia()
        {
            return View("Historia");
        }

        [ActionName("objetivos")]
        public ActionResult Objetivos()
        {
            return View("Objetivos");
        }

        [ActionName("pilares")]
        public ActionResult Pilares()
        {
            return View("Pilares");
        }
    }
}