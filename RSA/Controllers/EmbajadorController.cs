﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSA.Controllers
{
    public class EmbajadorController : Controller
    {
        // GET: Embajador
        public ActionResult Index()
        {
            return View();
        }

        [ActionName("perfil")]
        public ActionResult Perfil()
        {
            return View("Perfil");
        }

        [ActionName("nuestro-embajador")]
        public ActionResult NuestroEmbajador()
        {
            return View("NuestroEmbajador");
        }

        [ActionName("actividades")]
        public ActionResult Actividades()
        {
            return View("Actividades");
        }
    }
}