﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSA.Models;
using RSA.VM;

namespace RSA.Controllers
{
    public class CategoriaGaleriaController : Controller
    {
        GestionModel moduloModel = new GestionModel();

        // GET: Gestion
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AllCategoriaGaleria(ServRSA.VMGridCategoriaGaleriaGet modelo)
        {
            ServRSA.VMTableOfVMCategoriaGaleriai0e4Zjb6 model = moduloModel.CatalogoCategoriaGaleria(modelo);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ServRSA.VMCategoriaGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = moduloModel.RegistrarCategoriaGaleria(model);
                resp.success = true;
                resp.alerta = "Categoría de Galería registrada correctamente";
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Details(int id)
        {
            ServRSA.VMCategoriaGaleria model = moduloModel.GetCategoriaGaleria(id);
            return Json(new { model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(ServRSA.VMCategoriaGaleria model)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                moduloModel.EditarCategoriaGaleria(model);
                resp.success = true;
                resp.alerta = "Información actualizada correctamente";

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult ListCategoriaGaleria(bool todos = true)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {
                resp.result = moduloModel.ListarCategoriaGaleria(todos);
                resp.success = true;
            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }

            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }
    }
}