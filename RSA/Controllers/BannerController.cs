﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSA.VM;

namespace RSA.Controllers
{
    public class BannerController : Controller
    {
        [HttpPost]
        public JsonResult GetBanners(bool activos)
        {
            RespuestaModel resp = new RespuestaModel();
            try
            {

                resp.success = true;

            }
            catch (Exception ex)
            {
                resp.success = false;
                resp.mensaje = ex.Message;
            }


            return Json(new { model = resp }, JsonRequestBehavior.AllowGet);
        }
    }
}