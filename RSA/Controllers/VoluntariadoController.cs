﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSA.Controllers
{
    public class VoluntariadoController : Controller
    {
        // GET: Voluntariado
        public ActionResult Index()
        {
            return View();
        }
        [ActionName("conoce-mas")]
        public ActionResult ConoceMas()
        {
            return View("ConoceMas");
        }

        [ActionName("requisitos")]
        public ActionResult Requisitos()
        {
            return View("Requisitos");
        }

        [ActionName("registro")]
        public ActionResult Registro()
        {
            return View("Registro");
        }

    }
}