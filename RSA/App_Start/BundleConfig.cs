﻿using System.Web;
using System.Web.Optimization;

namespace RSA
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/vue").Include(
                      "~/Scripts/vue.js",
                      "~/Scripts/vue-resource.js"));

            bundles.Add(new ScriptBundle("~/bundles/lightbox").Include(
                        "~/Scripts/plugins/simple-lightbox/simple-lightbox.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsvue-home").Include(
                      "~/ViewScripts/Home/index-*"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsvue-layout").Include(
                "~/ViewScripts/Home/layout-*"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsvue-nosotros").Include(
                "~/ViewScripts/Nosotros/index-*"));

            bundles.Add(new ScriptBundle("~/bundles/scriptsvue-embajador").Include(
                "~/ViewScripts/Embajador/embajador-*"));

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                     "~/Scripts/main-*"));

            bundles.Add(new ScriptBundle("~/bundles/plugin").Include(
                    "~/Scripts/plugins/plugins.min.js",
                    "~/Scripts/plugins/jquery.themepunch.tools.min.js",
                    "~/Scripts/plugins/jquery.themepunch.revolution.min.js",
                    "~/Scripts/plugins/notify/notify.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/componentes").Include(
                    "~/Scripts/componentes/v-grid-1.1.8.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/filepond").Include(
                    "~/Scripts/plugins/filepond/plugins/filepond-plugin-file-encode/filepond-plugin-file-encode.min.js",
                    "~/Scripts/plugins/filepond/plugins/filepond-plugin-file-validate-size/filepond-plugin-file-validate-size.min.js",
                    "~/Scripts/plugins/filepond/plugins/filepond-plugin-image-exif-orientation/filepond-plugin-image-exif-orientation.min.js",
                    "~/Scripts/plugins/filepond/plugins/filepond-plugin-image-preview/filepond-plugin-image-preview.min.js",
                    "~/Scripts/plugins/filepond/filepond.js",
                    "~/Scripts/plugins/filepond/customfpmainslider.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                     // "~/Content/bootstrap.css",                      
                      "~/Content/plugins.min.css",
                      "~/Content/settings.css",
                      "~/Content/layers.css",
                      "~/Content/navigation.css",
                      "~/Content/style.css",
                      "~/Content/site-*",
                      "~/Content/plugins/simple-lightbox/simplelightbox.min.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/theme-faz").Include(                      
                      "~/Content/theme-skins/faz.css"//skin
                      ));

            bundles.Add(new StyleBundle("~/Content/filepond").Include(                      
                      "~/Content/plugins/filepond/filepond-plugin-image-preview.css",
                      "~/Content/plugins/filepond/filepond.css"
                      ));
        }
    }
}
