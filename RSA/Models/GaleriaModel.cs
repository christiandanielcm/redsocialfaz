﻿using RSA.ServRSA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace RSA.Models
{
    public class GaleriaModel
    {
        private string bindigDifusion = "BasicHttpBinding_IServiceDifusion";

        public ServRSA.VMTableOfVMGaleriai0e4Zjb6 CatalogoGaleria(ServRSA.VMGridGaleriaGet grid)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMTableOfVMGaleriai0e4Zjb6 resp = cliente.GetAllGaleria(grid);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMGaleria RegistrarGaleria(ServRSA.VMGaleria modulo)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMGaleria resp = cliente.RegistrarGaleria(modulo);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMGaleria GetGaleria(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMGaleriaDetalle respdt = cliente.GetInfoGaleria(id);
                ServRSA.VMGaleria resp = new ServRSA.VMGaleria();
                if (respdt != null)
                {
                    resp = new VMGaleria
                    {
                        IdGaleria = respdt.IdGaleria,
                        TituloGaleria = respdt.TituloGaleria,
                        UrlVideo = respdt.UrlVideo,
                        Descripcion = respdt.Descripcion,
                        FechaCaduca = respdt.FechaCaduca,
                        FechaRegistro = respdt.FechaRegistro,
                        IdCategoria = respdt.IdCategoria,
                        IdCausa = respdt.IdCausa,
                        Activo = respdt.Activo
                    };
                }

                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void EditarGaleria(ServRSA.VMGaleria modulo)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                cliente.EditaGaleria(modulo);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void EliminarGaleria(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                cliente.EliminarGaleria(id);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }


        #region Imagenes de la Galeria

        public ServRSA.VMTableOfVMImagenGaleriai0e4Zjb6 CatalogoImagenGaleria(ServRSA.VMGridImagenGaleriaGet grid)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMTableOfVMImagenGaleriai0e4Zjb6 resp = cliente.GetAllImagenGaleria(grid);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public VMListImagenGaleria[] ObtenerImagesGaleria(bool todas = true)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                VMListImagenGaleria[] resp = cliente.ListImagenGaleria(todas);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMImagenGaleria RegistrarImageGaleria(ServRSA.VMImagenGaleria modulo)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMImagenGaleria resp = cliente.RegistrarImagenGaleria(modulo);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMImagenGaleria GetImageGaleria(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMImagenGaleria resp = cliente.GetInfoImagenGaleria(id);

                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }
        public void ActualizarImageGaleria(ServRSA.VMImagenGaleria modulo)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                cliente.EditaImagenGaleria(modulo);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void EliminarImageGaleria(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                cliente.EliminarImagenGaleria(id);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void BajaImageGaleria(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                cliente.BajaImagenGaleria(id);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        #endregion
    }
}
