﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ServiceModel;
using System.ServiceModel.Security;

namespace RSA.Models
{
    public class AvisoModel
    {
        private string bindigDifusion = "BasicHttpBinding_IServiceDifusion";

        public ServRSA.VMTableOfVMAvisoi0e4Zjb6 CatalogoAviso(ServRSA.VMGridAvisoGet grid)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMTableOfVMAvisoi0e4Zjb6 resp = cliente.GetAllAviso(grid);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMAviso RegistrarAviso(ServRSA.VMAviso modelo)
        {
            try
            {    
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMAviso resp = cliente.RegistrarAviso(modelo);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMAviso GetAviso(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                ServRSA.VMAviso resp = cliente.GetInfoAviso(id);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void EditarAviso(ServRSA.VMAviso modelo)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                cliente.EditaAviso(modelo);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }
            
        public ServRSA.VMListAviso[] ListarAviso(bool todos)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMListAviso[] resp = cliente.ListAviso(todos);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void BajaAviso(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                cliente.BajaAviso(id);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

    }
}