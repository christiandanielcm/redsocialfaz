﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ServiceModel;
using System.ServiceModel.Security;

namespace RSA.Models
{
    public class GestionModel
    {
        private string bindigDifusion = "BasicHttpBinding_IServiceDifusion";

        public ServRSA.VMTableOfVMCategoriaGaleriai0e4Zjb6 CatalogoCategoriaGaleria(ServRSA.VMGridCategoriaGaleriaGet grid)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMTableOfVMCategoriaGaleriai0e4Zjb6 resp = cliente.GetAllCategoriaGaleria(grid);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMCategoriaGaleria RegistrarCategoriaGaleria(ServRSA.VMCategoriaGaleria modelo)
        {
            try
            {     
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMCategoriaGaleria resp = cliente.RegistrarCategoriaGaleria(modelo);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMCategoriaGaleria GetCategoriaGaleria(int id)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                ServRSA.VMCategoriaGaleria resp = cliente.GetInfoCategoriaGaleria(id);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public void EditarCategoriaGaleria(ServRSA.VMCategoriaGaleria modelo)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);

                cliente.EditaCategoriaGaleria(modelo);
                cliente.Close();
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

        public ServRSA.VMListCategoriaGaleria[] ListarCategoriaGaleria(bool todos)
        {
            try
            {
                ServRSA.ServiceDifusionClient cliente = new ServRSA.ServiceDifusionClient(bindigDifusion);
                ServRSA.VMListCategoriaGaleria[] resp = cliente.ListCategoriaGaleria(todos);
                cliente.Close();
                return resp;
            }
            catch (FaultException<ServRSA.ExceptionService> ex)
            {
                throw new Exception(ex.Detail.Mensaje);
            }
        }

    }
}