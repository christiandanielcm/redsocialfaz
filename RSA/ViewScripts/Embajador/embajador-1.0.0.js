﻿/* ================================================
----------------- galerias.js ------------- */
var global;
(function ($) {
    "use strict";
    new Vue({
        el: '#seccionGaleriasEmbajador',
        data:
        {
            urlsGaleria: {
                getItem: baseUrl("Galeria/Details"),
                getAll: baseUrl("Galeria/AllGalerias")
            },
            urlsImageGaleria: {
                getAll: baseUrl("Galeria/AllImages"),
                getItem: baseUrl("Galeria/DetailsImage")
            },
            errorLoadGaleriaEmbajador: "",
            galerias: new Array(),
            modelImagesGaleria: [],
            selectorImgGaleria: '.gallery_simple a',
            contGalerias: 0,
            categoriaEmbajador : 1
        },
        created: function () {
            this.loadGalerias();
        },
        computed: {
        },
        watch: {
            errorLoadGaleriaEmbajador: function (newErr) {
                console.error(newErr);
                return "";
            }
        },
        methods: {
            loadGalerias: function () {
                this.$http.post(this.urlsGaleria.getAll, { modelo: { Todas: true, Filtro: { IdCategoria: this.categoriaEmbajador, Activo: true } } }).then(function (response) {
                    var resp = response.body.model;
                    if (resp === null) {
                        this.errorLoadGaleriaEmbajador = "loadGalerias -> Error al leer las galerias ";
                    }
                    else {
                        var i = 0;
                        this.galerias = resp.Lista;
                        for (i = 0; i < this.galerias.length; i++) {
                            this.cargarImages(i, this.galerias[i].IdGaleria);
                        }
                    }

                }, function (err) {
                    this.errorLoadGaleriaEmbajador = "loadGalerias -> " + err.status + ' : ' + err.statusText;
                });
            },
            cargarImages: function (index, id) {
                this.$http.post(this.urlsImageGaleria.getAll, { modelo: { Todas: true, Filtro: { IdGaleria: id } } }).then(function (response) {
                    var resp = response.body.model;
                    if (typeof resp !== 'undefined') {
                        this.modelImagesGaleria = this.modelImagesGaleria.concat(resp.Lista);
                        this.contGalerias++;
                        if (this.contGalerias === this.galerias.length) {
                            var i = 0;
                            for (i = 0; i < this.modelImagesGaleria.length; i++) {
                                this.cargarImagenGaleria(this.modelImagesGaleria[i].IdImagen);
                            }
                            setTimeout(function (self) {
                                self.reiniciarDetalleImagen();
                            }, 200, this);
                        }
                    }
                }, function (err) {
                    this.errorRegistroGaleria = err.status + ' : ' + err.statusText;
                });
            },
            cargarImagenGaleria: function (id) {
                this.isLoading = true;

                this.$http.get(this.urlsImageGaleria.getItem, { params: { 'id': id } }).then(function (response) {
                    var resp = response.body.model;
                    if (typeof resp !== 'undefined') {                               
                        var i = 0;
                        for (i = 0; i < this.modelImagesGaleria.length; i++) {
                            if (this.modelImagesGaleria[i].IdImagen === id) {
                                if (this.modelImagesGaleria[i].BinaryImagen === undefined || this.modelImagesGaleria[i].BinaryImagen.length == 0) {
                                    this.modelImagesGaleria[i].BinaryImagen = resp.BinaryImagen;
                                }
                            }
                        }

                    }
                }, function (err) {
                    this.errorRegistroGaleria = err.status + ' : ' + err.statusText;
                    this.isLoading = false;
                });
            },
            reiniciarDetalleImagen: function () {
                console.log("ok");
                global.isotopeActivate();
                global.isotopeFilter();
                var galeria_simple = $(this.selectorImgGaleria).simpleLightbox({
                    alertError: false,
                    captions: true,
                    captionSelector: 'self',
                    showCounter: true,
                    loop: true
                });

                galeria_simple.on('error.simplelightbox', function (e) {
                    console.log(e);
                });

            }
        }
    });
})(jQuery);

function obtenerIdArray(objeto, key) {
    var resp = new Array();
    var i = 0;
    for (i = 0; i < objeto.length; i++) {
        resp.push(objeto[i][key]);
    }
    return resp;
}

