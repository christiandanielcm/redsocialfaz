﻿//$(function () {
//    new Vue({
//        el: '#body',
//        data: {
//        },
//        computed: {

//        },
//        created: function () {
//            var self = this;           
//        },
//        watch: {

//        },
//        methods: {            
//            post: function () {
//                this.win_edit.isLoading = true;
//                this.clearErrorWinCita();
//                this.win_edit.paciente = {};
//                this.$http.post(this.win_edit.urlBusqueda, this.win_edit.modelBusqueda).then(function (response) {
//                    var model = response.body.model;
//                    if (!model.success) {
//                        this.win_edit.error = model.mensaje;
//                    }
//                    else {
//                        this.win_edit.resultadoBusqueda = model.result;
//                        if (model.result != null) {
//                            if (model.result.length === 1) {
//                                this.seleccionarAfiliado(model.result[0]);
//                            }
//                        }
//                        this.win_edit.mensaje = model.mensaje;
//                    }
//                    this.win_edit.isLoading = false;
//                }, function (a, b, c) {
//                    this.error = a.mensaje;
//                    this.win_edit.isLoading = false;
//                });
//            },
//            get: function (m) {
//                this.win_edit.isLoading = true;
//                this.clearErrorWinCita();
//                this.$http.get(this.win_edit.urlGet + '/' + m).then(function (response) {
//                    var model = response.body.model;
//                    this.openWinCitaEdit();
//                    if (model === null) {
//                        this.win_edit.error = "No se encontro información";
//                    }
//                    else {
//                        for (var index in model) {
//                            if (typeof (this.win_edit.model[index]) != undefined || typeof (this.win_edit.model[index]) != null) {
//                                this.win_edit.model[index] = model[index];
//                            }
//                        }
//                        this.win_edit.model.FechaDeVencimiento = this.convertirFecha(this.win_edit.model.FechaDeVencimiento, true);
//                    }
//                    this.win_edit.isLoading = false;
//                }, function (a, b, c) {
//                    this.error = a.mensaje;
//                    this.win_edit.isLoading = false;
//                });
//            },            
//            convertirFecha: function (f, iso) {
//                return obtenerFecha(f, iso);
//            }            
//        }
//    });
//});

$(document).ready(function () {
    //Alerta RSAz , First Visit
    new Vue({
        el: '#banners_home',
        data:
        {
            urls: {
                get: baseUrl("Banner/GetBanners")
            },
            errorBanner: "",
            banners: []
        },
        created: function () {
            this.loadBannersFV();
        },
        computed: {
        },
        watch: {
        },
        methods: {
            loadBannersFV: function () { //First Visit
                // Aviso primera visita
                if (document.cookie.indexOf('visited=true') === -1) {

                    this.$http.post(this.urls.get, { activos: true }).then(function (response) {
                        var resp = response.body.model;
                        if (resp.success === false) {
                            if (resp.mensaje !== null && resp.mensaje.length > 0) {
                                this.errorAlerta = "loadBanner -> " + resp.mensaje;
                            }
                        }
                        else {
                            if (typeof resp !== 'undefined') {
                                //this.alerta = resp.model.result;
                                //Por ahora le coloque uno obj para pruebas. 
                                this.banners.push({
                                    IdBanner: "1",
                                    TipoImagen: ".jpg",
                                    Url: "#"
                                }
                                );
                                //this.banners.push({
                                //    IdBanner: "2",
                                //    TipoImagen: ".jpg",
                                //    Url: "#"
                                //}
                                //);
                                setTimeout(function () {
                                    //Mostrar alerta
                                    $('#banners_home').modal('toggle');
                                    var year = 1000 * 60 * 60 * 24 * 365;
                                    var expires = new Date((new Date()).valueOf() + year);
                                    document.cookie = "visited=true;expires=" + expires.toUTCString();
                                }, 200, this);

                            }
                        }
                    }, function (err) {
                        this.errorBanner = "loadBanner -> " + err.status + ' : ' + err.statusText;
                    });


                }
            },
            active: function (index, classaux) {
                var cssvalue = [classaux];
                if (index === 0) {
                    cssvalue.push('active');
                }
                return cssvalue.join(' ');
            }
        }
    });

    //Slider VUE  rev_slider_wrapper
    if ($("#rev_slider_wrapper").length !== 0) {
        new Vue({
            el: '#rev_slider_wrapper',
            data:
            {
                urls: {
                    get: baseUrl("Aviso/Details"),
                    getAll: baseUrl("Aviso/ListAviso")
                },
                errorRegistroAviso: "",
                avisos: new Array()
            },
            created: function () {
                //setTimeout(function (self) {
                //    self.filePondObj =
                //        FilePond.create(($('#fileaviso')[0]), {
                //            allowMultiple: false,
                //            labelIdle: 'Arrastra y suelta una foto o <span class="filepond--label-action">Selecciona...</span>',
                //            imagePreviewHeight: 300,
                //            imageCropAspectRatio: '1:1',
                //            imageResizeTargetWidth: 200,
                //            imageResizeTargetHeight: 200,
                //            imagePreviewTransparencyIndicator: '#1BA160',
                //            instantUpload: false
                //        });
                //}, 1000, this);
                this.loadAvisos();
            },
            computed: {
            },
            watch: {
                errorRegistroAviso: function (newErr) {
                    console.error(newErr);
                    return "";
                }
            },
            methods: {
                loadAvisos: function () {
                    this.$http.post(this.urls.getAll, { todos: false }).then(function (response) {
                        var resp = response.body.model;
                        if (resp.success === false) {
                            if (resp.mensaje !== null && resp.mensaje.length > 0) {
                                this.errorRegistroAviso = "loadAviso -> " + resp.mensaje;
                            }
                        }
                        else {
                            if (resp.alerta !== null && resp.alerta.length > 0) {
                                $.notify(resp.alerta, "success");
                            }
                            this.avisos = resp.result;
                            //setTimeout(function () {
                            //    var revapi;
                            //    if ($("#rev_slider").revolution === undefined) {
                            //        revslider_showDoubleJqueryError("#rev_slider");
                            //    } else {
                            //        revapi = $("#rev_slider").show().revolution({
                            //            sliderType: "standard",
                            //            jsFileLocation: "Scripts/",
                            //            sliderLayout: "auto",
                            //            dottedOverlay: "none",
                            //            delay: 15000,
                            //            navigation: {
                            //                mouseScrollNavigation: "off",
                            //                onHoverStop: "off",
                            //                touch: {
                            //                    touchenabled: "on"
                            //                },
                            //                arrows: {
                            //                    style: "custom",
                            //                    enable: true,
                            //                    hide_onmobile: false,
                            //                    hide_under: 768,
                            //                    hide_onleave: false,
                            //                    tmp: '',
                            //                    left: {
                            //                        h_align: "left",
                            //                        v_align: "center",
                            //                        h_offset: 0,
                            //                        v_offset: 0
                            //                    },
                            //                    right: {
                            //                        h_align: "right",
                            //                        v_align: "center",
                            //                        h_offset: 0,
                            //                        v_offset: 0
                            //                    }
                            //                },
                            //                bullets: {
                            //                    enable: true,
                            //                    style: "hades",
                            //                    hide_onmobile: false,
                            //                    hide_onleave: false,
                            //                    hide_under: 768,
                            //                    direction: "horizontal",
                            //                    h_align: "center",
                            //                    v_align: "bottom",
                            //                    h_offset: 0,
                            //                    v_offset: 15,
                            //                    space: 5,
                            //                    tmp: '<span class="tp-bullet-image"></span>'
                            //                }
                            //            },
                            //            responsiveLevels: [1200, 992, 768, 480],
                            //            gridwidth: [1140, 970, 750, 480],
                            //            gridheight: [500, 440, 360, 300],
                            //            lazyType: "smart",
                            //            spinner: "spinner2",
                            //            parallax: {
                            //                type: "off",
                            //                origo: "slidercenter",
                            //                speed: 2000,
                            //                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                            //                disable_onmobile: "on"
                            //            },
                            //            debugMode: false
                            //        });
                            //    }
                            //}, 1000);


                            console.log(this.avisos);
                        }
                    }, function (err) {
                        this.errorRegistroAviso = "loadAviso -> " + err.status + ' : ' + err.statusText;
                    });
                }

            }
        });

    }


});



/* =================================================
----------------- Main.js ------------- */
var global;
(function ($) {
    "use strict";
    //Valida que exista la sección
    if ($("#seccionGalerias").length !== 0) {
        new Vue({
            el: '#seccionGalerias',
            data:
            {
                urlsGaleria: {
                    getItem: baseUrl("Galeria/Details"),
                    getAll: baseUrl("Galeria/AllGalerias")
                },
                urlsImageGaleria: {
                    getAll: baseUrl("Galeria/AllImages"),
                    getItem: baseUrl("Galeria/DetailsImage")
                },
                errorRegistroAviso: "",
                galerias: new Array(),
                modelImagesGaleria: [],
                selectorImgGaleria: '.gallery_simple a',
                contGalerias: 0
            },
            created: function () {
                //setTimeout(function (self) {
                //    self.filePondObj =
                //        FilePond.create(($('#fileaviso')[0]), {
                //            allowMultiple: false,
                //            labelIdle: 'Arrastra y suelta una foto o <span class="filepond--label-action">Selecciona...</span>',
                //            imagePreviewHeight: 300,
                //            imageCropAspectRatio: '1:1',
                //            imageResizeTargetWidth: 200,
                //            imageResizeTargetHeight: 200,
                //            imagePreviewTransparencyIndicator: '#1BA160',
                //            instantUpload: false
                //        });
                //}, 1000, this);
                this.loadGalerias();
            },
            computed: {
            },
            watch: {
                errorRegistroAviso: function (newErr) {
                    console.error(newErr);
                    return "";
                }
            },
            methods: {
                loadGalerias: function () {
                    this.$http.post(this.urlsGaleria.getAll, { modelo: { Todas: true, Filtro: { IdCategoria: null, Activo: true } } }).then(function (response) {
                        var resp = response.body.model;
                        if (resp === null) {
                            this.errorRegistroAviso = "loadGalerias -> Error al leer las galerias ";
                        }
                        else {
                            var i = 0;
                            this.galerias = resp.Lista;
                            setTimeout(function () {
                                //global.isotopeActivate();
                                //global.isotopeFilter();
                            }, 5000);

                            for (i = 0; i < this.galerias.length; i++) {
                                this.cargarImages(i, this.galerias[i].IdGaleria);
                            }
                        }

                    }, function (err) {
                        this.errorRegistroAviso = "loadGalerias -> " + err.status + ' : ' + err.statusText;
                    });
                },
                cargarImages: function (index, id) {
                    this.$http.post(this.urlsImageGaleria.getAll, { modelo: { Todas: true, Filtro: { IdGaleria: id } } }).then(function (response) {
                        var resp = response.body.model;
                        if (typeof resp !== 'undefined') {
                            this.modelImagesGaleria = this.modelImagesGaleria.concat(resp.Lista);
                            this.contGalerias++;
                            if (this.contGalerias === this.galerias.length) {
                                var i = 0;
                                for (i = 0; i < this.modelImagesGaleria.length; i++) {
                                    this.cargarImagenGaleria(this.modelImagesGaleria[i].IdImagen);
                                }
                                setTimeout(function (self) {
                                    self.reiniciarDetalleImagen();
                                }, 200, this);
                            }
                        }
                    }, function (err) {
                        this.errorRegistroGaleria = err.status + ' : ' + err.statusText;
                    });
                },
                cargarImagenGaleria: function (id) {
                    this.isLoading = true;

                    this.$http.get(this.urlsImageGaleria.getItem, { params: { 'id': id } }).then(function (response) {
                        var resp = response.body.model;
                        if (typeof resp !== 'undefined') {
                            //No se usa find por incopatibilidad en servidores                               
                            //if (resp.BinaryImagen.length === 0) {
                            //    resp.BinaryImagen = 'undefined';
                            //}                                    
                            var i = 0;
                            for (i = 0; i < this.modelImagesGaleria.length; i++) {
                                if (this.modelImagesGaleria[i].IdImagen === id) {
                                    if (this.modelImagesGaleria[i].BinaryImagen === undefined || this.modelImagesGaleria[i].BinaryImagen.length === 0) {
                                        this.modelImagesGaleria[i].BinaryImagen = resp.BinaryImagen;
                                    }
                                }
                            }

                        }
                    }, function (err) {
                        this.errorRegistroGaleria = err.status + ' : ' + err.statusText;
                        this.isLoading = false;
                    });
                },
                reiniciarDetalleImagen: function () {
                    console.log("ok");
                    global.isotopeActivate();
                    global.isotopeFilter();
                    var galeria_simple = $(this.selectorImgGaleria).simpleLightbox({
                        alertError: false,
                        captions: true,
                        captionSelector: 'self',
                        showCounter: true,
                        loop: true
                    });

                    galeria_simple.on('error.simplelightbox', function (e) {
                        console.log(e);
                    });

                }
            }
        });
    }


    //Añadio : CC
    //Fecha : 28/11/2018
    //Seccion de Acciones de Voluntariado

    //const item_carrusell_vol = {
    //    template: "<div class='item item_carrusel'" +
    //        " v-bind:style='{'background':'url(' + '@Href(\"~/Images/galerias/_\")'+idGaleria+'/ '+'_'+idImagen+(tipoImagen==null?'.png':tipoImagen) + ') no-repeat center center fixed', 'background-size':  'cover' }' ></div>",
    //    props: ['idGaleria', 'idImagen', 'tipoImagen']
    //};
    //Valida que exista la sección
    if ($("#seccionAccionesVoluntariado").length !== 0) {
        new Vue({
            el: '#seccionAccionesVoluntariado',
            //components: {
            //    item_carrusell_vol
            //}, 
            //template: '<div> ' +
            //    '<text-component v-for="(item, index) in modelImagesAccionesVoluntariado"' +
            //    ':class="{ \'active\': index === 0 }"' +
            //    ':idGaleria="item.IdGaleria"' +
            //    ':idImagen="item.IdImagen"' +
            //    ':tipoImagen="item.TipoImagen"' +
            //    '>' +
            //    '</text-component>' +
            //'</div>',
            data:
            {
                urlsVoluntariado: {
                    getAllVoluntariado: baseUrl("Galeria/AllGalerias"),
                    getAll: baseUrl("Galeria/AllImages"),
                    getItem: baseUrl("Galeria/DetailsImage")
                },
                errorCargaAccionesVoluntariado: "",
                accionesVoluntariado: new Array(),
                modelImagesAccionesVoluntariado: [],
                contAcciones: 0,
                idCategoriaVoluntariado: 3 //VOLUNTARIADO
            },
            created: function () {
                this.loadAccionesVoluntariado();
            },
            computed: {
            },
            methods: {
                loadAccionesVoluntariado: function () {
                    this.$http.post(this.urlsVoluntariado.getAllVoluntariado, { modelo: { Todas: true, Filtro: { IdCategoria: parseInt(this.idCategoriaVoluntariado), Activo: true } } }).then(function (response) {
                        var resp = response.body.model;
                        if (resp === null) {
                            this.errorRegistroAviso = "loadAccionesVoluntariado -> Error al leer / cargar las galerias del voluntariado ";
                        }
                        else {
                            var i = 0;
                            this.accionesVoluntariado = resp.Lista;//Se toma una por ahora, falta definir como manejarlas en caso de que sean mas
                            for (i = 0; i < this.accionesVoluntariado.length; i++) {
                                this.cargarImagesVoluntariado(this.accionesVoluntariado[i].IdGaleria);
                            }
                        }

                    }, function (err) {
                        this.errorRegistroAviso = "loadAccionesVoluntariado -> " + err.status + ' : ' + err.statusText;
                    });
                },
                cargarImagesVoluntariado: function (id) {
                    this.$http.post(this.urlsVoluntariado.getAll, { modelo: { Todas: true, Filtro: { IdGaleria: id }, Activo: true } }).then(function (response) {
                        var resp = response.body.model;
                        if (typeof resp !== 'undefined') {
                            this.modelImagesAccionesVoluntariado = resp.Lista;
                        }
                    }, function (err) {
                        this.errorCargaAccionesVoluntariado = err.status + ' : ' + err.statusText;
                    });
                },
                cssItem: function (index) {
                    var cssvalue = ['item item_carrusel'];
                    if (index === 0) {
                        cssvalue.push('active');
                    }
                    return cssvalue.join(' ');
                }
            }
        });

    }
    var Simple = {
        initialised: false,
        mobile: false,
        container: $('#portfolio-item-container'),
        blogContainer: $('#blog-item-container'),
        init: function () {

            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }


            this.pageLoadAnim();
            this.checkMobile();
            this.menuHover();
            this.mobileMenuDropdownFix();
            this.menuOnClick();
            this.stickyHeader();
            this.overlayMenuToggle();
            this.overlayMenuDropdownFix();
            this.sideMenu();
            this.sideMenuToggle();
            this.productZoom();
            this.scrollToTop();
            //CC
            this.ir_a();
            this.twitterFeed();
            this.flickerFeed();
            this.instagramFeed();
            this.progressBars();
            this.scrollAnimations();
            this.tooltip();
            this.popover();
            this.simplelightbox();

            if ($.fn.owlCarousel) {
                this.owlCarousels();
            }


            this.countTo();


            if (typeof noUiSlider === "object") {
                this.filterSliders();
            }


            if ($.fn.lightGallery) {
                this.lightBox();
            }


            if ($.fn.matchHeight) {
                this.matchProducts();
            }

            var self = this;

            if (typeof imagesLoaded === 'function') {
                self.container.imagesLoaded(function () {
                    //self.isotopeActivate();
                    //self.isotopeFilter();
                });


                self.blogContainer.imagesLoaded(function () {
                    self.blogMasonry();
                });
            }

        },
        checkMobile: function () {

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                this.mobile = true;
            } else {
                this.mobile = false;
            }
        },
        pageLoadAnim: function () {

            if ($('#page-loader').length) {
                $('#page-loader').delay(700).fadeOut(800, function () {
                    $(this).remove();
                });
            }
        },
        menuHover: function () {
            if (typeof Modernizr === "object" && Modernizr.mq('only all and (min-width: 768px)') && !Modernizr.touchevents) {
                if ($.fn.hoverIntent) {
                    $('.header').find('.navbar-nav').not('.nav-overlay').hoverIntent({
                        over: function () {
                            var $this = $(this);

                            $this.addClass('open');
                            if ($this.find('ul, div').length) {
                                $this.find('.dropdown-toggle').addClass('disabled');
                            }
                        },
                        out: function () {
                            var $this = $(this);

                            $this.removeClass('open');
                            if ($this.hasClass('open')) {
                                $this.find('.dropdown-toggle').removeClass('disabled');
                            }
                        },
                        selector: 'li',
                        timeout: 100,
                        interval: 40
                    });
                }
            }
        },
        mobileMenuDropdownFix: function () {
            if (typeof Modernizr === "object" && (Modernizr.mq('only all and (max-width: 767px)') || Modernizr.touchevents)) {
                $('.navbar-nav').not('.nav-overlay').find('.dropdown-toggle').on('click', function (e) {
                    var parent = $(this).closest('li');
                    // close all the siblings and their children
                    parent.siblings().removeClass('open').find('li').removeClass('open');
                    // open which one is clicked
                    parent.toggleClass('open');

                    // prevent
                    e.preventDefault();
                    e.stopPropagation();
                });
            }
        },
        menuOnClick: function () {
            var self = this;
            // Menu on click scroll animation for onepages
            $('.onepage-nav').find('a').on('click', function (e) {
                var target = $(this).attr('href');
                if (target.indexOf('#') === -1 || !$(target).length) {
                    return;
                }

                var elem = $(target),
                    targetPos = elem.offset().top;

                $('html, body').animate({
                    'scrollTop': targetPos
                }, 1200);
                e.preventDefault();
            });
        },
        stickyHeader: function () {
            // Sticky header - calls if sticky-header class is added to the header
            if ($('.sticky-header').length && $(window).width() >= 992) {
                var sticky = new Waypoint.Sticky({
                    element: $('.sticky-header')[0],
                    stuckClass: 'fixed',
                    offset: -400
                });
            }
        },
        overlayMenuToggle: function () {
            // Overlay Menu Show/Hide via .nav-open class
            $('.menu-toggle').on('click', function (e) {
                $('.navbar-container').toggleClass('nav-open');
                e.preventDefault();
            });
        },
        overlayMenuDropdownFix: function () {
            // Overlay menu sub dropdown toggle fix
            $('.nav-overlay').find('.dropdown-toggle').on('click', function (e) {
                var parent = $(this).closest('li');
                // close all the siblings and their children
                parent.siblings().removeClass('open').find('li').removeClass('open');
                // open which one is clicked
                parent.toggleClass('open');

                // prevent
                e.preventDefault();
                e.stopPropagation();
            });
        },
        sideMenu: function () {
            if ($.fn.metisMenu) {
                $('.side-menu').metisMenu();
            }
        },
        sideMenuToggle: function () {
            // Overlay Menu Show/Hide via .nav-open class
            $('.sidemenu-toggle').on('click', function (e) {
                $('.header-inside').toggleClass('open');
                e.preventDefault();
            });
        },
        owlCarousels: function () {

            /* RSA FAZ Home */
            $('.causas_faz').owlCarousel({
                loop: true,
                margin: 30,
                responsiveClass: true,
                nav: true,
                navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
                dots: false,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    420: {
                        items: 2
                    },
                    768: {
                        items: 4
                    }
                }
            });


            /* Carrusel en modal detalle de causa */
            $('.causas_modal.owl-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin: 0,
                responsiveClass: true,
                nav: true,
                navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
                dots: false,
                autoplay: true,
                autoplayTimeout: 6000
            });

            /* Carrusel de organizaciones registradas en RSA */
            $('.organizaciones_rsa.owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                responsiveClass: true,
                nav: true,
                navText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
                dots: false,
                autoplay: true,
                autoplayTimeout: 4000,
                responsive: {
                    0: {
                        items: 2,
                        margin: 10
                    },
                    420: {
                        items: 3,
                        margin: 10
                    },
                    768: {
                        items: 4,
                        margin: 15
                    },
                    992: {
                        items: 5
                    }
                }
            });
        },
        tooltip: function () {
            // Bootstrap Tooltip
            if ($.fn.tooltip) {
                $('[data-toggle="tooltip"]').tooltip();
            }
        },
        popover: function () {
            // Bootstrap Popover
            if ($.fn.popover) {
                $('[data-toggle="popover"]').popover({
                    trigger: 'focus'
                });
                $('[data-imagen="popover"]').popover({
                    html: true,
                    trigger: 'hover',
                    content: function () {
                        return '<img src="' + $(this).data('img') + '" class="img-thumbnail img-responsive" />';
                    }
                });
            }
        },
        scrollBtnAppear: function () {
            if ($(window).scrollTop() >= 400) {
                $('#scroll-top').addClass('fixed');
            } else {
                $('#scroll-top').removeClass('fixed');
            }
        },
        scrollToTop: function () {
            $('#scroll-top').on('click', function (e) {
                $('html, body').animate({
                    'scrollTop': 0
                }, 1200);
                e.preventDefault();
            });
        },
        ir_a: function () {

            $('.ir_a').on('click', function (e) {
                var section = $(this).attr("href");
                $('html, body').animate({
                    scrollTop: $(section).offset().top
                }, 1200);
                e.preventDefault();
            });
        },
        lightBox: function () {
            /* Galerria de Imagenes FAZ. */
            $('.popup-gallery').lightGallery({
                selector: '.zoom-btn',
                thumbnail: true,
                exThumbImage: 'data-thumb',
                thumbWidth: 50,
                thumbContHeight: 60
            });

            // Lightbox for video with button - see: index-agency
            $('.video-btn-section').lightGallery({
                selector: '.trigger-video-btn',
                thumbnail: false
            });
        },
        simplelightbox: function () {

        },
        productZoom: function () {
            var self = this;
            // Product page zoom plugin settings
            if ($.fn.elevateZoom) {
                $('#product-zoom').elevateZoom({
                    responsive: true,
                    zoomType: 'inner', // lens or window can be used - options already set below
                    borderColour: '#e1e1e1',
                    zoomWindowPosition: 1,
                    zoomWindowOffetx: 30,
                    cursor: "crosshair", //
                    zoomWindowFadeIn: 400,
                    zoomWindowFadeOut: 250,
                    lensBorderSize: 3, // lens border size
                    lensOpacity: 1,
                    lensColour: 'rgba(255, 255, 255, 0.5)', // lens color
                    lensShape: "square", // circle lens shape can be uses
                    lensSize: 200,
                    scrollZoom: true
                });

                /* swap images for zoom on click event */
                $('.product-gallery').find('a').on('click', function (e) {
                    var ez = $('#product-zoom').data('elevateZoom'),
                        smallImg = $(this).data('image'),
                        bigImg = $(this).data('zoom-image');

                    ez.swaptheimage(smallImg, bigImg);
                    e.preventDefault();
                });
            }
        },
        progressBars: function () {
            var self = this;
            // Calculate and Animate Progress 
            $('.progress-animate').waypoint(function (direction) {
                var $this = $(this.element),
                    progressVal = $this.data('width');

                $this.css({ 'width': progressVal + '%' }, 400);
            }, {
                    offset: '90%',
                    triggerOnce: true
                });
        },
        countTo: function () {
            // CountTo plugin used count animations for homepages
            if ($.fn.countTo) {
                if ($.fn.waypoint) {
                    $('.count').waypoint(function () {
                        $(this.element).countTo();
                    }, {
                            offset: '90%',
                            triggerOnce: true
                        });
                } else {
                    $('.count').countTo();
                }
            } else {
                // fallback if count plugin doesn't included
                // Get the data-to value and add it to element
                $('.count').each(function () {
                    var $this = $(this),
                        countValue = $this.data('to');
                    $this.text(countValue);
                });
            }
        },
        scrollAnimations: function () {
            /* Wowy Plugin */
            if (typeof WOW === 'function') {
                new WOW({
                    boxClass: 'wow',      // default
                    animateClass: 'animated', // default
                    offset: 0          // default
                }).init();
            }
        },
        twitterFeed: function () {
            /* Twitter feed for user*/
            if ($.fn.tweet && $('.twitter-feed-widget').length) {
                $('.twitter-feed-widget').tweet({
                    modpath: './assets/js/twitter/',
                    avatar_size: '',
                    count: 2,
                    query: 'wrapbootstrap', // change query with username if you want to display search results
                    loading_text: 'searching twitter...',
                    join_text: '',
                    retweets: false,
                    template: '<div class="twitter-icon"><i class="fa fa-twitter"></i></div><div class="tweet-content">{text}{time}</div>'
                    /* etc... */
                });
            }
        },
        flickerFeed: function () {
            /* Flickr feed plugin  */
            // credits https://www.flickr.com/photos/smanography/
            if ($.fn.jflickrfeed) {
                $('.flickr-widget-list').jflickrfeed({
                    limit: 12,
                    qstrings: {
                        id: '56502208@N00' // change with you flickr id
                    },
                    itemTemplate: '<li>' + '<a href="{{image}}" target="_blank" title="{{title}}">' + '<img src="{{image_s}}" alt="{{title}}" />' + '</a>' + '</li>'
                });
            }
        },
        instagramFeed: function () {
            // Instagram Feed
            if ($.fn.spectragram && $('#instafeed').length) {

                jQuery.fn.spectragram.accessData = {
                    accessToken: '2229187323.566f1cf.c41eaca370664379b822dc3b17bb1464',
                    clientID: '7c28e44736494357ba3df343b1c699fe'
                };

                jQuery('#instafeed').spectragram('getUserFeed', {
                    query: 'eonythemes',
                    max: 13,
                    size: 'medium',
                    wrapEachWith: '',
                    complete: function () {

                        $('#instafeed.owl-carousel').owlCarousel({
                            loop: true,
                            margin: 0,
                            responsiveClass: true,
                            nav: false,
                            dots: false,
                            autoplay: true,
                            autoplayTimeout: 15000,
                            smartSpeed: 800,
                            responsive: {
                                0: {
                                    items: 3
                                },
                                480: {
                                    items: 4
                                },
                                768: {
                                    items: 6
                                },
                                992: {
                                    items: 7
                                },
                                1200: {
                                    items: 9
                                },
                                1500: {
                                    items: 10
                                },
                                1900: {
                                    items: 12
                                }
                            }
                        });
                    }
                });
            }
        },
        filterSliders: function () {
            // Slider For category pages / filter price
            var priceSlider = document.getElementById('price-slider');

            // Check if #price-slider elem is exists if not return
            // to prevent error logs
            if (priceSlider === null) return;

            noUiSlider.create(priceSlider, {
                start: [100, 900],
                connect: true,
                step: 50,
                range: {
                    'min': 0,
                    'max': 1000
                }
            });

            this.sliderText(priceSlider, '$');
        },
        sliderText: function (slider, currency) {
            // add slider values as a text 
            // check for currency too
            var currencyVar = (currency) ? '$' : null,
                divHandles = slider.getElementsByClassName('noUi-handle'),
                divs = [];

            // Add divs to the slider handles.
            for (var i = 0; i < divHandles.length; i++) {
                divs[i] = document.createElement('div');
                divHandles[i].appendChild(divs[i]);
            }

            // When the slider changes, write the value to the tooltips.
            slider.noUiSlider.on('update', function (values, handle) {
                divs[handle].innerHTML = (currencyVar) ? (currencyVar + values[handle]) : Math.round(values[handle]);
            });
        },
        isotopeActivate: function () {
            // Trigger for isotope plugin
            if ($.fn.isotope) {
                var container = this.container,
                    layoutMode = container.data('layoutmode');

                container.isotope({
                    itemSelector: '.portfolio-item',
                    layoutMode: (layoutMode) ? layoutMode : 'masonry'
                });
            }
        },
        isotopeReinit: function () {
            // Recall for isotope plugin
            if ($.fn.isotope) {
                this.container.isotope('destroy');
                this.isotopeActivate();
            }
        },
        isotopeFilter: function () {
            // Isotope plugin filter handle
            var self = this,
                filtersContainer = $('#portfolio-filter, #nav-portfolio-filter');

            filtersContainer.find('a').on('click', function (e) {
                var $this = $(this),
                    selector = $this.attr('data-filter');

                filtersContainer.find('.active').removeClass('active');

                // And filter now
                self.container.isotope({
                    filter: selector,
                    transitionDuration: '0.8s'
                });

                $this.closest('li').addClass('active');
                e.preventDefault();
            });
        },
        blogMasonry: function () {
            // Trigger for isotope plugin
            if ($.fn.isotope) {
                var blogContainer = this.blogContainer;

                blogContainer.isotope({
                    itemSelector: '.entry-grid',
                    layoutMode: 'masonry'
                });
            }
        },
        matchProducts: function () {
            // Match all products (Category - Shop Pages)
            $('.products-container').each(function () {
                $(this).find('.product').matchHeight();
            });
        }
    };
    global = Simple;
    // Ready Event
    jQuery(document).ready(function () {
        // Init our app
        Simple.init();
    });

    // Load Event
    $(window).on('load', function () {
        Simple.scrollBtnAppear();
    });

    // Scroll Event
    $(window).on('scroll', function () {
        Simple.scrollBtnAppear();
    });


    // Google Map api v3 - Map for contact pages
    if (document.getElementById("map") && typeof google === "object") {
        // Map pin coordinates and content of pin box
        var locations = [
            [
                '<address><strong>Address:</strong> Hollywood Blvd, Los Angeles, CA, USA <br> <strong>Phone:</strong> +01 010 554 11 22 </address>',
                34.101780,
                -118.333655
            ]
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: new google.maps.LatLng(34.101780, -118.333655), // Map Center coordinates
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();


        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: 'assets/images/pin.png'
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }

})(jQuery);

function obtenerIdArray(objeto, key) {
    var resp = new Array();
    var i = 0;
    for (i = 0; i < objeto.length; i++) {
        resp.push(objeto[i][key]);
    }
    return resp;
}


