﻿
$(document).ready(function () {
    //Alerta RSAz , First Visit
    new Vue({
        el: '#banners_home',
        data:
        {
            urls: {
                get: baseUrl("Banner/GetBanners")
            },
            errorBanner: "",
            banners: []
        },
        created: function() {
            this.loadBannersFV();
        },
        computed: {
        },
        watch: {
        },
        methods: {
            loadBannersFV: function() { //First Visit
                // Aviso primera visita
                if (document.cookie.indexOf('visited=true') === -1) {

                    this.$http.post(this.urls.get, { activos: true }).then(function(response) {
                            var resp = response.body.model;
                            if (resp.success === false) {
                                if (resp.mensaje != null && resp.mensaje.length > 0) {
                                    this.errorAlerta = "loadBanner -> " + resp.mensaje;
                                }
                            } else {
                                if (typeof resp !== 'undefined') {
                                    //this.alerta = resp.model.result;
                                    //Por ahora le coloque uno obj para pruebas. 
                                    this.banners.push({
                                            IdBanner: "1",
                                            TipoImagen: ".jpg",
                                            Url: "#"
                                        }
                                    );
                                    //this.banners.push({
                                    //    IdBanner: "2",
                                    //    TipoImagen: ".jpg",
                                    //    Url: "#"
                                    //}
                                    //);
                                    setTimeout(function() {
                                            //Mostrar alerta
                                            $('#banners_home').modal('toggle');
                                            var year = 1000 * 60 * 60 * 24 * 365;
                                            var expires = new Date((new Date()).valueOf() + year);
                                            document.cookie = "visited=true;expires=" + expires.toUTCString();
                                        },
                                        200,
                                        this);

                                }
                            }
                        },
                        function(err) {
                            this.errorBanner = "loadBanner -> " + err.status + ' : ' + err.statusText;
                        });


                }
            },
            active: function(index, classaux) {
                var cssvalue = [classaux];
                if (index === 0) {
                    cssvalue.push('active');
                }
                return cssvalue.join(' ');
            }
        }
    });
});