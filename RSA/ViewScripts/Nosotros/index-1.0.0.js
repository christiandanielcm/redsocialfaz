﻿//===== nosostros js =======
Vue.component('page-header',
    {
        props: ['title', 'image', 'lists'],
        template: '<div class="page-header larger parallax custom" ' +
            'v-bind:style="{\'background\': \'url(\'+ image + \') no-repeat center center\', \'background-size\': \'cover\'}">' +
            '<div class= "container"><h1>{{title}}</h1>' +
            '<ol class="breadcrumb">' +
            '<li v-for="item in lists"><a v-bind:href="item.url">{{item.value}}</a></li>' +
            '</ol>' +
            '</div>' +
            '</div>',
    });
var leftsdm = Vue.component('left-sidebar-menu',
    {
        props: ['title', 'lists'],
        template: '<div class= "widget">' +
            '<h3 class= "widget-title"> {{ title }}</h3>' +
            '<ul class="fa-ul">' +
            '<li v-for="item in lists"><a v-on:click="loadPartialPage(item)"><i class="fa-li fa fa-chain"></i>{{ item.value }}</a></li>' +
            '</ul>' +
            '</div>',
        methods: {
            clicked(event) {

            }
        }
    });

var global;
(function ($) {

    "use strict";
    new Vue({
        el: '#nosotros',
        data:
        {
            items_menu: [
                {
                    value: "¿Quienes Somos?",
                    url: baseUrl("Nosotros")
                },
                {
                    value: "Historia",
                    url: baseUrl("Nosotros")
                }, {
                    value: "Objetivos",
                    url: baseUrl("Nosotros")
                }, {
                    value: "Pilares",
                    url: baseUrl("Nosotros")
                }
            ],
            title: "",
            breadcrumbs: new Array(),
            errorRegistroAviso: ""

        },
        created: function () {
            this.breadcrumbs[0] = { value: "Nosotros", url: "#" }
            this.loadPartialPage(this.items_menu[0]);
        },
        computed: {
        },
        watch: {
            errorNosotros: function (newErr) {
                console.error(newErr);
                return "";
            }
        },
        methods: {
            loadPartialPage: function (item) {
                this.title = item.value;
                this.breadcrumbs[1] = item;
            }
        },
        components: {
            'left-sidebar-menu': leftsdm
        }
    });
})(jQuery);