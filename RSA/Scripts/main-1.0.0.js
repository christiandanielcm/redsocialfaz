﻿
function obtenerFecha(stringFecha, isIso) {
    var fecha = "";
    try {
        //"/[(]+[0-9]+[)]/g"
        var exp = new RegExp("[(]+[0-9]+[)]");
        if (exp.test(stringFecha)) {
            var f = (exp.exec(stringFecha))[0];
            if (isIso == true) {
                fecha = (new Date(parseInt(f.replace("(", "").replace(")", "")))).toISOString();
                fecha = fecha.substring(0, fecha.indexOf("T"))
            }
            else {
                fecha = (new Date(parseInt(f.replace("(", "").replace(")", "")))).toLocaleDateString();
            }
        }
    } catch{
        fecha = stringFecha;
    }
    finally {
        return fecha;
    }
}