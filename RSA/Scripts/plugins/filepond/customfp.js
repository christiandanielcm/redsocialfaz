﻿
FilePond.registerPlugin(
    FilePondPluginFileEncode,
    FilePondPluginFileValidateSize,
    FilePondPluginImageExifOrientation,
    FilePondPluginImagePreview
);

FilePond.create(
    document.querySelector('input[type=file]'),
    {
        labelIdle: 'Arrastra y suelta una foto o <span class="filepond--label-action">Selecciona...</span>',
        imagePreviewHeight: 170,
        imageCropAspectRatio: '1:1',
        imageResizeTargetWidth: 200,
        imageResizeTargetHeight: 200,
        imagePreviewTransparencyIndicator: '#1BA160'
    }
);

